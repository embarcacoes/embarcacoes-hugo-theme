# LKCAMP Hugo Theme

This is the hugo theme we use for our [website](https://lkcamp.dev).

# Features

- Posts
- Events
- Alerts
- Adaptive

# How to use it

From the root of your Hugo project directory, run the following commands:

```
git submodule add https://gitlab.com/lkcamp/lkcamp-hugo-theme themes/lkcamp
git submodule update --init --recursive
```

Then, set your theme to `lkcamp` in the `config.toml` file:

```
theme = "lkcamp"
```

Now, to create new content, use the `hugo new` command with the available
[archetypes](./archetypes).

# Credit

Many thanks to [Ícaro Chiabai](https://github.com/icarochiabai),
[Tim "Mobybit"](https://github.com/mobybit), and [Jonathan
Rutheiser](https://github.com/jrutheiser), on whose work this project was built
upon.

# License

This project is licensed under the GPLv3.
